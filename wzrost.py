import numpy as np
from cec2017.functions import f1, f2, f3
import matplotlib.pyplot as plt
from autograd import grad

def booth(x):
    x1, x2 = x
    return (x1 + 2 * x2 - 7) ** 2 + (2 * x1 + x2 - 5) ** 2

setups = [(booth, 0.01), (f1, 0.00000005), (f2, 0.008), (f3, 0.000005)]

MAX_X = 100
PLOT_STEP = 0.1
f, β = setups[3]

x = np.random.uniform(-MAX_X, MAX_X, size=2)
grad_fct = grad(f)
steps = [x.copy()]

for i in range(1000):
    x -= grad_fct(x) * β
    steps.append(x.copy())

x_arr = np.arange(-MAX_X, MAX_X, PLOT_STEP)
y_arr = np.arange(-MAX_X, MAX_X, PLOT_STEP)
X, Y = np.meshgrid(x_arr, y_arr)
Z = np.empty(X.shape)

for i in range(X.shape[0]):
    for j in range(X.shape[1]):
        Z[i, j] = f(np.array([X[i, j], Y[i, j]]))

plt.contour(X, Y, Z, 20)

for i in range(0, len(steps)-1):
    plt.arrow(steps[i][0], steps[i][1], steps[i+1][0] - steps[i][0], steps[i+1][1] - steps[i][1])

plt.show()
