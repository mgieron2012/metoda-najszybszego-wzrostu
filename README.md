# Metoda najszybszego wzrostu

## Zadanie

Zaimplementować metodę najszybszego wzrostu. Gradient wyliczamy numerycznie.
Narysować zachowanie algorytmu (kolejne kroki algorytmu jako strzałki na tle poziomic funkcji celu). Uwaga: w praktycznych zadaniach optymalizacji nie da się narysować funkcji celu ponieważ zadania mają wiele wymiarów (np. 100), oraz koszt wyznaczenia oceny jednego punktu jest duży.
Zastosować metodę do znalezienia optimum funkcji booth, po czym do znalezienia optimum funkcji o numerach od 1 do 3 z CEC 2017.

## Pytania

Jak wartość parametru beta wpływa na szybkość dojścia do optimum i zachowanie algorytmu?
Zalety/wady algorytmu?

## Odpowiedzi

Im większy parametr beta, tym szybciej wartość funkcji będzie zbiegała do ekstremum.
Przy dużych wartościach bety kroki mogą być na tyle duże, że ekstremum nie zostanie znalezione.

Zalety:
- prostota implementacji
- można zwiększyć dokładność wyniku poprzez zwiększenie liczby iteracji

Wady:
- konieczność ustalenia parametru beta, od którego zależy poprawność działania algorytmu
- przy skomplikowanych funkcjach trudno określić, czy wynik działania algorytmu jest poprawny, co utrudnia dobranie parametru beta
- nie daje gwarancji znalenienia ekstremum globalnego

Metoda najszybszego wzrostu dobrze radzi sobie ze znajdowaniem ekstremów lokalnych funkcji,
wymaga jednak dobrania parametrów dla prawie każdej analizowanej funkcji.

Znalezienie minimum funkcji booth:
![Booth.jpg](booth.png)
